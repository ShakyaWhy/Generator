# Avris Generator

Generate and validate numbers and identifiers like IBAN, ISBN, EAN, PESEL, BSN, UTR, NINO, NIF, DNI, passwords...

## GUI & API

    yarn
    yarn build
    yarn start
    
## Library

    yarn add avris-generator
    const gm = require('avris-generator');
    
    const pesel = gm.generate('PL', 'pesel', {gender: 'm'});
    
    try {
        const data = gm.validate('PL', 'pesel', '12345678900');
        // valid
    } catch (e) {
        // invalid
        alert(e.message);
    }
    
## Copyright

* **Author:** Andrea [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
