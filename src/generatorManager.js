class GeneratorManager {
    constructor(generators) {
        this.generators = generators;
    }

    generate(country, generator, params = {}) {
        return this.get(country, generator).generate(params);
    }

    validate(country, generator, value) {
        return this.get(country, generator).validate(value);
    }

    get(country, generator) {
        if (!(country in this.generators)) {
            throw new Error(`Country ${country} not found`);
        }

        if (!(generator in this.generators[country])) {
            throw new Error(`Generator ${generator} not found`);
        }

        return this.generators[country][generator];
    }

    all() {
        return this.generators;
    }
}

module.exports = GeneratorManager;
