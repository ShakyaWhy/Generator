const express = require('express');
const router = express.Router();
const createError = require('http-errors');
const generators = require('../src/generators');

const countries = Object.keys(generators).join('|');

router.get(`/:country(|${countries})`, function(req, res, next) {
    res.render('index', {
        generators: generators,
        baseUrl: req.protocol + '://' + req.get('host'),
        country: req.params.country.toUpperCase(),
        links: {
            website: {href: 'https://avris.it', icon: 'far fa-user', text: 'Andrea'},
            email: {href: 'mailto:andrea@avris.it', icon: 'far fa-envelope', text: 'andrea@avris.it'},
            twitter: {href: 'https://twitter.com/AvrisIT', icon: 'fab fa-twitter', text: '@AvrisIT'},
            gitlab: {href: 'https://gitlab.com/Avris/Generator', icon: 'fab fa-gitlab', text: 'Avris/Generator'},
            npm: {href: 'https://www.npmjs.com/package/avris-generator', icon: 'fab fa-npm', text: 'NPM'},
            paypal: {href: 'https://paypal.me/AndreAvris', icon: 'fab fa-paypal', text: 'PayPal'},
            bitcoin: {href: 'https://www.blockchain.com/btc/address/155pGeHNCu4wzxHZwW3JzzZeDonkbi2jnE', icon: 'fab fa-bitcoin', text: 'Bitcoin'},
        },
        apiEndpoints: {
            'index.get': {method: 'GET', url: '/api'},
            'generate.get': {method: 'GET', url: '/api/:country/:generator?param=value'},
            'validate.get': {method: 'GET', url: '/api/:country/:generator/:number'},
            'barcode.svg': {method: 'GET', url: '/api/barcode/ean13/:number.svg'},
        },
    });
});

module.exports = router;
