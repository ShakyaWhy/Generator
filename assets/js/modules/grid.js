const $ = require('jquery');

class Columnist {
    constructor($container) {
        this.$container = $container;
        this.timer = null;
    }

    layout() {
        const columns = this._countColumns();

        const margins = {};
        let i = 0;
        this.$container.childNodes.forEach($col => {
            const style = this._getStyle($col);
            if (!style) {
                return;
            }

            const extraHeight = parseInt(style.marginBottom.slice(0, -2), 10)
                + parseInt(style.paddingBottom.slice(0, -2), 10)
                + parseInt(style.borderBottomWidth.slice(0, -2), 10);

            margins[i] = Math.max(0, $col.offsetHeight - $col.childNodes[0].offsetHeight - extraHeight);

            if (margins[i - columns] !== undefined) {
                $col.style.marginTop = `-${margins[i - columns]}px`;
            }

            i++;
        });
    }

    _countColumns() {
        const columnWidth = this._getColumnWidth();

        return columnWidth ? Math.floor(this.$container.offsetWidth / columnWidth) : 1;
    }

    _getColumnWidth() {
        for (let $col of this.$container.childNodes) {
            if (this._getStyle($col)) {
                return $col.offsetWidth;
            }
        }

        return null;
    }

    _getStyle($el) {
        if (!($el instanceof Element)) {
            return null;
        }
        const style = $el.currentStyle || window.getComputedStyle($el);
        if (style.display === 'none') {
            return null;
        }

        return style;
    }

    start(interval = 300) {
        this.timer = setInterval(() => this.layout(), interval);
    }

    destroy() {
        if (this.timer) {
            clearInterval(this.timer);
            this.timer = null;
        }

        this.$container.childNodes.forEach(($col, i) => {
            $col.style.marginTop = null;
        });
    }
}

const grid = document.querySelector('.grid');
let columnist = null;

if (grid) {
    columnist = new Columnist(grid);
    columnist.start(100);
}

const $filterBtns = $('[data-filter-target]');

$filterBtns.click(function() {
    const $btn = $(this);
    const $target = $($btn.data('filter-target'));
    const value = $btn.data('filter-value');
    const on = value && $btn.hasClass('active');
    const expected = on ? '' : value;

    $filterBtns.removeClass('active');
    $filterBtns.filter(`[data-filter-value="${value}"]`).toggleClass('active', !on);

    window.history.pushState(null, null, on ? '/' : $btn.attr('href'));

    $target.find('[data-filter-value]').each(function () {
        const $el = $(this);
        const stays = expected === '' || $el.data('filter-value') === expected;
        $el.toggleClass('grid-item-hidden', !stays)
    });

    if (columnist) {
        columnist.destroy();
        columnist.start();
    }

    $btn.blur();

    return false;
});

$('[data-filter-init]').each(function() {
    const $filter = $(this);
    const init = $filter.data('filter-init');
    if (init) {
        $filter.find(`[data-filter-value="${init}"]`).click();
    }
});
