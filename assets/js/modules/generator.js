const $ = require('jquery');
require('select2');
const twemoji = require('twemoji').default;

const generators = require('../../../src/generators');
const t = require('../../helpers/translator');
const flag = require('../../helpers/flag');

const dataDisplay = function(key, value) {
    switch (key) {
        case 'country':
            return twemoji.parse(flag(value)) + ' ' + t('country.' + value);
        case 'countries':
            return value.map((country) => '<span class="text-nowrap">' + twemoji.parse(flag(country)) + ' ' + t('country.' + country) + '</span>').join(', ');
        case 'region':
            const [country, region] = value;
            return t(`region.${country}.${region}`);
        case 'regions':
            return value.map(([country, region]) => '<span class="text-nowrap">' + t(`region.${country}.${region}`) + '</span>').join(', ');
        case 'gender':
            const genderCodes = {
                m: 'male',
                f: 'female',
                x: 'diverse',
            };
            return value.split('').map(v => t('gender.' + genderCodes[v])).join(', ');
        case 'birthdate':
            if (typeof(value) === 'string') {
                return value;
            }

            const year = value.getUTCFullYear();
            const month = (value.getUTCMonth() + 1).toString().padStart(2, '0');
            const day = value.getUTCDate().toString().padStart(2, '0');

            return `${year}-${month}-${day}`;
        case 'type':
            return t('type.' + value);
        default:
            return value;
    }
};

$('.generator').each(function() {
    const $t = $(this);
    const generator = generators[$t.data('country')][$t.data('generator')];
    const $field = $t.find('.generator-output');
    const $generateBtn = $t.find('.btn-generate');
    const $feedback = $t.find('.feedback');

    $t.find('[data-param]').on('change keyup', function(e) {
        const $p = $(this);

        $p.removeClass('is-valid is-invalid');
        if (!this.checkValidity()) {
            $p.addClass('is-invalid');
            return;
        } else {
            $p.addClass('is-valid');
        }

        if (e.keyCode === 13) {
            return $generateBtn.click();
        }
    }).trigger('change');

    $generateBtn.click(e => {
        const params = {};
        $t.find('[data-param]').each(function(i, el) {
            const $el = $(el);
            const param = $el.data('param');
            if (!(param in params)) {
                params[param] = '';
            }
            if ($el.is('[type=text]') || $el.is('[type=number]') || $el.is(':checked') || $el.is('select')) {
                params[param] += $el.val();
            }
        });

        const value = generator.generate(params);
        $field.val(value).trigger('change');
        if (e.originalEvent !== undefined) {
            return $field.focus().select();
        }
    });

    $field.on('change keyup', e => {
        $field.removeClass('is-valid is-invalid');
        $feedback.removeClass('valid-feedback invalid-feedback');
        $feedback.html('');

        try {
            const data = generator.validate($field.val());
            $field.addClass('is-valid');
            $feedback.addClass('valid-feedback');
            if (data === null) {
                return;
            }

            $feedback.html('<strong>' + t('valid') + '</strong>');

            if (typeof data === 'object') {
                for (let key in data) {
                    if (data.hasOwnProperty(key)) {
                        if (key === 'barcode') {
                            $feedback.append(`<div class="barcode">${data[key]}</div>`);
                        } else {
                            $feedback.append(`<br/>${t('valid.data.' + key)}: ${dataDisplay(key, data[key])}`);
                        }
                    }
                }
            }
        } catch (ex) {
            $field.addClass('is-invalid');
            $feedback.addClass('invalid-feedback');
            $feedback.html('<strong>' + t('invalid.' + ex.message) + '</strong>');
        }
        if (e.keyCode === 13) {
            return $generateBtn.click();
        }
    });

    $t.find('select[data-countries]').each(function () {
        const $select = $(this);
        $select.append(`<option value=""></option>`);
        for (let countryCode in generator.constructor.countries) {
            if (generator.constructor.countries.hasOwnProperty(countryCode)) {
                $select.append(`<option value="${countryCode}">${flag(countryCode)} ${t('country.' + countryCode)}</option>`);
            }
        }

        const entryTemplate = function(entry) {
            return $(twemoji.parse('<span>' + entry.text + '</span>'));
        };

        $select.select2({
            width: '100%',
            theme: 'bootstrap4',
            templateResult: entryTemplate,
            templateSelection: entryTemplate,
            allowClear: true,
            placeholder: { id: "", text: "" },
        });
    });

    $t.find('select[data-regions]').each(function () {
        const $select = $(this);
        $select.append(`<option value=""></option>`);
        for (let regionCode in generator.constructor.regions) {
            if (generator.constructor.regions.hasOwnProperty(regionCode)) {
                $select.append(`<option value="${regionCode}">${t(`region.${$select.data('country')}.${regionCode}`)}</option>`);
            }
        }

        $select.select2({
            width: '100%',
            theme: 'bootstrap4',
            allowClear: true,
            placeholder: { id: "", text: "" },
        });
    });

    $t.find('select[data-map]').each(function () {
        const $select = $(this);
        const field = $select.data('map');
        $select.append(`<option value=""></option>`);
        for (let key in generator.constructor[field]) {
            if (generator.constructor[field].hasOwnProperty(key)) {
                $select.append(`<option value="${key}">${generator.constructor[field][key]}</option>`);
            }
        }

        $select.select2({
            width: '100%',
            theme: 'bootstrap4',
            allowClear: true,
            placeholder: { id: "", text: "" },
        });
    });
});

$('.btn-generate').click();
$('.generator-output').blur();
