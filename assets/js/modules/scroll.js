const $ = require('jquery');

$('a[href^="#"]').click(function () {
    const $target = $($(this).attr('href'));
    if ($target.length) {
        $('html, body').animate({scrollTop: $target.position().top}, 200);
        return false;
    }
});
